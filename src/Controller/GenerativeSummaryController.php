<?php

namespace Drupal\generative_summary\Controller;

use Drupal\Core\Controller\ControllerBase;
use OpenAI\Client;
use Rajentrivedi\TokenizerX\TokenizerX;

/**
 * Defines the Generative Summary Controller.
 *
 * This class provides methods to generate summaries using OpenAI's API.
 */
class GenerativeSummaryController extends ControllerBase {

  /**
   * Counts the number of tokens in a given text.
   *
   * @param string $text
   *   The text to be tokenized.
   *
   * @return int
   *   The number of tokens in the text.
   */
  protected function countTokens($text): int {
    return TokenizerX::count($text);
  }

  /**
   * Split text into smaller chunks.
   *
   * @param array|string $q
   *   The text to be split.
   * @param mixed $maxChunkTokens
   *   The maximum number of tokens in each chunk.
   *
   * @return array
   *   The split text.
   */
  public function textSplitter(array|string $q, mixed $maxChunkTokens): array {
    // Treat $q as plain text.
    $sentences = \explode(".", $q);
    $currentSegment = "";
    $currentTokens = 0;

    foreach ($sentences as $sentence) {
      $tokens = $this->countTokens($sentence);
      if ($currentTokens + $tokens > $maxChunkTokens) {
        $segments[] = \trim($currentSegment);
        $currentSegment = "";
        $currentTokens = 0;
      }
      // Prepend "." when adding a new sentence from 2nd round.
      if (!empty($currentSegment)) {
        $currentSegment .= ". ";
      }
      $currentSegment .= $sentence;
      $currentTokens += $tokens;
    }

    // For the remaining part of the text.
    if (!empty($currentSegment)) {
      $segments[] = \trim($currentSegment);
    }
    return $segments;
  }

  /**
   * Checks if a given string is HTML.
   *
   * @param string $string
   *   The string to check.
   *
   * @return bool
   *   TRUE if the string is HTML, FALSE otherwise.
   */
  protected function isHtml($string): bool {
    return \preg_match("/<[^<]+>/", $string, $m) != 0;
  }

  /**
   * Set credentials.
   *
   * @param array $options
   *   The client build options.
   *
   * @return \OpenAI\Client
   *   The OpenAI client.
   */
  public function openAiClient($options): Client {
    try {
      if ($options['openai_api_service'] == 'azure_openai') {
        $client = \OpenAI::factory()
          ->withBaseUri($options['openai_api_base_url'])
          ->withHttpHeader('api-key', $options['openai_api_key'])
          ->withQueryParam('api-version', $options['openai_api_version'])
          ->make();
      }
      else {
        $org = !empty($options['openai_api_org']) ? $options['openai_api_org'] : NULL;
        if (!empty($options['openai_api_key'])) {
          $client = \OpenAI::client($options['openai_api_key'], $org);
        }
        else {
          $this->messenger()->addMessage($this->t('Missing or incorrect OpenAI API Key for Generative Summary module.'), 'error');
        }
      }
    }
    catch (\Exception $e) {
      $this->getLogger('generative_summary')->error($e->getMessage());
    }
    return $client;
  }

  /**
   * Fetches a summary from the OpenAI API.
   *
   * @param array $options
   *   The client build options.
   * @param string $body
   *   The body of text to summarize.
   *
   * @return string
   *   The generated summary.
   */
  public function doRequest($options, $body): string {
    $client = $this->openAiClient($options);
    $payload = [
      'model' => $options['openai_model'],
      'messages' => [
      [
        'role' => 'system',
        'content' => $options['custom_prompt_system_role'],
      ],
      [
        'role' => 'user',
        'content' => $options['custom_prompt_user_role'] . $body,
      ],
      ],
      'temperature' => 0.5,
      'max_tokens' => \intval($options['max_length']),
    ];

    try {
      // Use the "openai-php/client" library to make the API call.
      $response = $client->chat()->create($payload);

      // Extract the summarized content from the response.
      $summary = $response->choices[0]->message->content ?? '';

      return $summary;
    }
    catch (\Exception $e) {
      $this->getLogger('generative_summary')->error('Failed generating summary: %message', ['%message' => $e->getMessage()]);
      return '';
    }
  }

  /**
   * Generates a summary for a given body of text.
   *
   * @param string $body
   *   The body of text to summarize.
   * @param array $options
   *   The options for the summary generation.
   *
   * @return string
   *   The generated summary.
   */
  private function generateSummary($body, $options): string {
    $chunks = $this->textSplitter(\strip_tags($body), $options['max_chunk_tokens']);

    return $this->doRequest($options, $chunks[0]);
  }

  /**
   * Generates a summary from a given text.
   *
   * @param string $body
   *   The body of text to summarize.
   * @param int $max_length
   *   The maximum length of the summary.
   * @param int $max_sentences
   *   The maximum number of sentences in the summary.
   * @param int $max_chunk_tokens
   *   The maximum number of tokens in each chunk.
   * @param string $custom_prompt_system_role
   *   The system role custom prompt.
   * @param string $custom_prompt_user_role
   *   The user role custom prompt.
   *
   * @return string
   *   The generated summary.
   */
  public function generateSummaryFromText($body, $max_length, $max_sentences, $max_chunk_tokens, $custom_prompt_system_role, $custom_prompt_user_role): string {
    $options = [];
    // Get global settings as fallback.
    $config = $this->config('generative_summary.settings');
    $options['openai_api_service'] = $config->get('openai_api_service');
    $options['openai_api_key'] = $config->get('openai_api_key');
    $options['openai_api_base_url'] = $config->get('openai_api_base_url');
    $options['openai_api_version'] = $config->get('openai_api_version');
    $options['openai_api_org'] = $config->get('openai_api_org');
    $options['openai_model'] = $config->get('openai_model');

    $options['max_length'] = !empty($max_length) ? $max_length : $config->get('default_max_length');
    $options['max_sentences'] = !empty($max_sentences) ? $max_sentences : $config->get('default_max_sentences');
    $options['max_chunk_tokens'] = !empty($max_chunk_tokens) ? $max_chunk_tokens : $config->get('default_max_chunk_tokens');
    $options['custom_prompt_system_role'] = !empty($custom_prompt_system_role) ? $custom_prompt_system_role : $config->get('default_custom_prompt_system_role');
    $options['custom_prompt_user_role'] = !empty($custom_prompt_user_role) ? $custom_prompt_user_role : $config->get('default_custom_prompt_user_role');

    // Replace tokens: {{ length }} and {{ sentences }}.
    $options['custom_prompt_user_role'] = \str_replace('{{ length }}', $options['max_length'], $options['custom_prompt_user_role']);
    $options['custom_prompt_user_role'] = \str_replace('{{ sentences }}', $options['max_sentences'], $options['custom_prompt_user_role']);

    return $this->generateSummary($body, $options);
  }

}
