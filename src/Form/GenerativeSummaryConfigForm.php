<?php

namespace Drupal\generative_summary\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class for the Generative Summary configuration form.
 */
class GenerativeSummaryConfigForm extends ConfigFormBase {
  /**
   * OpenAI chat completion models.
   *
   * @var array
   * The GPT models available for completion.
   */
  protected array $models = [
    "GPT-3.5" => [
      "gpt-3.5-turbo" => "gpt-3.5-turbo",
      "gpt-3.5-turbo-16k" => "gpt-3.5-turbo-16k",
    ],
    "GPT-4" => [
      "gpt-4" => "gpt-4",
      "gpt-4-32k" => "gpt-4-32k",
    ],
  ];

  /**
   * {@inheritDoc}
   */
  public function getFormId(): string {
    return 'generative_summary_config';
  }

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames(): array {
    return ['generative_summary.settings'];
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('generative_summary.settings');

    $form['openai_api_service'] = [
      '#type' => 'select',
      '#title' => $this->t('Service Provider'),
      '#required' => TRUE,
      '#options' => [
        'openai' => $this->t('OpenAI'),
        'azure_openai' => $this->t('Microsoft Azure OpenAI'),
      ],
      '#default_value' => !empty($config->get('openai_api_service')) ? $config->get('openai_api_service') : 'openai',
    ];

    $form['openai_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key for OpenAI Chat Completions API'),
      '#default_value' => $config->get('openai_api_key'),
      '#required' => FALSE,
    ];

    $form['openai_api_org'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Organization'),
      '#default_value' => !empty($config->get('openai_api_org')) ? $config->get('openai_api_org') : '',
      '#states' => [
        'visible' => [
          ':input[name="openai_api_service"]' => ['value' => 'openai'],
        ],
      ],
    ];
    $form['openai_api_base_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Azure OpenAI Base URL'),
      '#default_value' => !empty($config->get('openai_api_base_url')) ? $config->get('openai_api_base_url') : '',
      '#description' => $this->t('Format example: {your-resource-name}.openai.azure.com/openai/deployments/{deployment-id}'),
      '#states' => [
        'visible' => [
          ':input[name="openai_api_service"]' => ['value' => 'azure_openai'],
        ],
      ],
    ];

    $form['openai_api_version'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Azure OpenAI Version'),
      '#default_value' => !empty($config->get('openai_api_version')) ? $config->get('openai_api_version') : '',
      '#states' => [
        'visible' => [
          ':input[name="openai_api_service"]' => ['value' => 'azure_openai'],
        ],
      ],
    ];

    $form['openai_model'] = [
      '#type' => 'select',
      '#title' => $this->t('Model'),
      '#description' => $this->t('The model which will generate the completion. <a href="%link" target="_blank">Learn more</a>.', ['%link' => 'https://platform.openai.com/docs/models']),
      '#options' => $this->models,
      '#default_value' => !empty($config->get('openai_model')) ? $config->get('openai_model') : 'gpt-3.5-turbo',
      '#required' => TRUE,
    ];

    $form['default_min_length'] = [
      '#type' => 'number',
      '#title' => $this->t('Default min length of body text for summary'),
      '#description' => $this->t('The default minimum length of text required to generate a summary. Leave empty to use the default value.'),
      '#default_value' => $config->get('default_max_length') ?? 0,
      '#min' => 0,
    ];

    $form['default_max_length'] = [
      '#type' => 'number',
      '#title' => $this->t('Default max length of summary'),
      '#description' => $this->t('The default maximum length for generated summaries.'),
      '#default_value' => $config->get('default_max_length') ?? 150,
    ];

    $form['default_max_sentences'] = [
      '#type' => 'number',
      '#title' => $this->t('Default max number of sentences'),
      '#description' => $this->t('The default maximum number of sentences for generated summaries.'),
      '#default_value' => $config->get('default_max_sentences') ?? 2,
    ];

    $form['default_max_chunk_tokens'] = [
      '#type' => 'number',
      '#title' => $this->t('Default max number of tokens per chunk'),
      '#description' => $this->t('The default maximum number of tokens per chunk.'),
      '#default_value' => $config->get('default_max_chunk_tokens') ?? 1000,
    ];

    $form['default_custom_prompt_system_role'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Custom prompt of system role'),
      '#description' => $this->t('Custom prompt for the system role in the conversation.'),
      '#default_value' => $config->get('default_custom_prompt_system_role') ?? 'You are a senior and mature digital media editor who knows how to write an attractive summary and extract the most representative info from a scientific article.',
    ];

    $form['default_custom_prompt_user_role'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Custom prompt of user role'),
      '#description' => $this->t('Custom prompt for the user role in the conversation. Available tokens: {{ length }} and {{ sentences }}.'),
      '#default_value' => $config->get('default_custom_prompt_user_role') ?? 'Please summarize the text in one to {{ sentences }} sentences only. The total characters must be {{ length }} characters or less: ',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('generative_summary.settings')
      ->set('default_min_length', $form_state->getValue('default_min_length'))
      ->set('default_max_length', $form_state->getValue('default_max_length'))
      ->set('default_max_sentences', $form_state->getValue('default_max_sentences'))
      ->set('default_max_chunk_tokens', $form_state->getValue('default_max_chunk_tokens'))
      ->set('default_custom_prompt_system_role', $form_state->getValue('default_custom_prompt_system_role'))
      ->set('default_custom_prompt_user_role', $form_state->getValue('default_custom_prompt_user_role'))
      ->set('openai_api_service', $form_state->getValue('openai_api_service'))
      ->set('openai_api_key', $form_state->getValue('openai_api_key'))
      ->set('openai_api_base_url', $form_state->getValue('openai_api_base_url'))
      ->set('openai_api_version', $form_state->getValue('openai_api_version'))
      ->set('openai_model', $form_state->getValue('openai_model'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
